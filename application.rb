require_relative 'contact'
require_relative 'contact_database'
require 'pry'

class Application

def user_choice(query)
  if query == 'help'
    help_list
  elsif query == 'new'
    get_user_input
  elsif query == 'list'
    Contact.all
  end
end 


def help_list
  puts "new  - Create a new contact"
  puts "list - List all contacts"
  puts "show - Show a contact"
  puts "find - Find a contact"
end

def create_list
end


def get_user_input
  prompt ='>'
   @new_values =[]
  puts "Please enter the name of the contact"
  print prompt
  @name = STDIN.gets.chomp()
  puts "Please enter the email address of the contact"
  print prompt
  @email = STDIN.gets.chomp()
  @new_values<< @name << @email
  Contact.create(@new_values) 
end

end

new_app = Application.new

new_app.user_choice(ARGV[0])
