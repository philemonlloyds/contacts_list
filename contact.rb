require_relative 'contact_database'

require 'pry'
require 'csv'


class Contact
  attr_accessor :name, :email
  def initialize(name, email)
    @@contacts = ContDatabase.current_contact_list
    # puts @@contacts
    # TODO: assign local variables to instance variables
    @name = name
    @email = email
  end
 
  def to_s
    # TODO: return string representation of Contact

  end
 
  ## Class Methods
  class << self
  def create(newvalues)
    # TODO: Will initialize a contact as well as add it to the list of contact
     # add_contacts(newvalues)
    # CSV.open('contacts.csv', 'a') do |csv|
    ContDatabase.add_contacts(newvalues)
    # end
  end
 
    def find(index)
      # TODO: Will find and return contact by index
    end
 
    def all
      ContDatabase.current_contact_list
    end
    
    def show(id)
      # TODO: Show a contact, based on ID
    end
    
  end

end
